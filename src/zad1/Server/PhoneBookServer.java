package zad1.Server;

import zad1.Common.PhoneDirectory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.rmi.RemoteException;
import java.util.Hashtable;

public class PhoneBookServer {
    public static void main(String[] args)
    {
        try {
            PhoneDirectory phoneDirectory;
            phoneDirectory= new PhoneDirectory(args[0]);
            Hashtable env = new Hashtable(11);
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
            Context initCtx = new InitialContext(env);

            initCtx.rebind("PhoneDirectoryService", phoneDirectory);

        } catch (RemoteException | NamingException e) {
            e.printStackTrace();
        }
    }
}
