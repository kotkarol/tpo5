package zad1.Common;

import java.rmi.RemoteException;

public interface PhoneDirectoryInterface extends java.rmi.Remote {

    Object getPhoneNumber(String phoneNumber) throws RemoteException;

    boolean addPhoneNumber(String name, String num) throws RemoteException;

    boolean replacePhoneNumber(String name, String num) throws RemoteException;
}
