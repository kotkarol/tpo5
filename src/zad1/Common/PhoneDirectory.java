package zad1.Common;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import javax.rmi.PortableRemoteObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class PhoneDirectory extends PortableRemoteObject implements PhoneDirectoryInterface, Referenceable {

    private Map<String, String> pbMap = new HashMap<>();
    private String fileName;

    /**
     * Inicjalna zawartość książki telefonicznej
     * jest wczytywana z pliku o formacie
     * imię  numer_telefonu
     *
     * @param fileName
     */
    public PhoneDirectory(String fileName) throws RemoteException {
        super();
        this.setFileName(fileName);
        try {
            BufferedReader br = new BufferedReader(
                    new FileReader(fileName));
            String line;
            while ((line = br.readLine()) != null) {
                String[] info = line.split(" +", 2);
                pbMap.put(info[0], info[1]);
            }
        } catch (Exception exc) {
            exc.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Zwraca numer telefonu dla podanej osoby
     *
     * @param name
     * @return
     */
    public String getPhoneNumber(String name) throws RemoteException {
        return pbMap.get(name);
    }

    /**
     * Dodaje nową osobę do książki
     * Wynik:
     * - true - dodana
     * - false - nie (przy próbie dodania osoby zapisanej już w książce)
     *
     * @param name
     * @param num
     * @return
     */
    public boolean addPhoneNumber(String name, String num) throws RemoteException {
        if (pbMap.containsKey(name)) return false;
        pbMap.put(name, num);
        return true;
    }

    /**
     * Zastępuje numer podanej osoby nowym
     * Wynik:
     * - true (numer zastąpiony)
     * - false (nie - próba podania nowegu numeru nieistniejącej osoby)
     *
     * @param name
     * @param num
     * @return
     */
    public boolean replacePhoneNumber(String name, String num) throws RemoteException {
        if (!pbMap.containsKey(name)) return false;
        pbMap.put(name, num);
        return true;
    }

    /**
     * Retrieves the Reference of this object.
     *
     * @return The non-null Reference of this object.
     * @throws NamingException If a naming exception was encountered
     *                         while retrieving the reference.
     */
    @Override
    public Reference getReference() throws NamingException {
        return new Reference(PhoneDirectory.class.getName(), new StringRefAddr("phoneDirectory",
                this.getFileName()), PhoneDirectoryFactory.class.getName(), null); // factory location
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}