package zad1.Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import zad1.Common.PhoneDirectoryInterface;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import java.util.Hashtable;

public class PhoneBookClient extends Application {
    public static PhoneDirectoryInterface PhoneDirecotryInterface;

    public static void  main( String args[] ) {
        Context ic;
        Object objref;

        try {
            IntializeRemote();
            launch(args);
            // STEP 2: Narrow the object reference to the concrete type and
            // invoke the method.

            //hi.addPhoneNumber("karol" , "123123123123");

        } catch( Exception e ) {
            System.err.println( "Exception " + e);
            e.printStackTrace( );
        }
    }

    private static void IntializeRemote() throws NamingException {
        Hashtable env = new Hashtable(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        Context ic  = new InitialContext(env);
        Object objref = ic.lookup("PhoneDirectoryService");
        PhoneDirecotryInterface = (PhoneDirectoryInterface) PortableRemoteObject.narrow(
                objref, PhoneDirectoryInterface.class);
    }

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set. The primary stage will be embedded in
     *                     the browser if the application was launched as an applet.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages and will not be embedded in the browser.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Views/PhoneBookClientView.fxml"));
        primaryStage.setTitle("TPO 5 - Client");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
