package zad1.Client.Controllers;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import zad1.Client.PhoneBookClient;

import java.rmi.RemoteException;

public class PhoneBookClientController {

    @FXML
    public Label GetResult;
    @FXML
    public javafx.scene.control.TextField AddName;
    @FXML
    public javafx.scene.control.TextField AddNewNumber;
    @FXML
    public javafx.scene.control.Button GetButton;
    @FXML
    public TextField LookedNumber;
    @FXML
    public TextField ReplaceName;
    @FXML
    public TextField ReplaceNumber;

    @FXML
    public void initialize() {
        AddNewNumber.setPadding(new Insets(0, 0, 30, 0));
    }

    public void GetNumberByName() throws RemoteException {
        Object obj = PhoneBookClient.PhoneDirecotryInterface.getPhoneNumber(LookedNumber.getText());
        if (obj == null) {
            GetResult.setText("-----");
        } else {
            GetResult.setText(obj.toString());
        }
        LookedNumber.setText("");
    }

    public void AddNewNumber() throws RemoteException {
        String newName = AddName.getText();
        String newNumber = AddNewNumber.getText();

        if (newName == null || newNumber == null) {
            return;
        }

        if (newName.isEmpty() || newNumber.isEmpty()) {
            return;
        }

        PhoneBookClient.PhoneDirecotryInterface.addPhoneNumber(newName, newNumber);
        AddName.setText("");
        AddNewNumber.setText("");
    }

    public void ReplaceNumberClicked() throws RemoteException {
        String replaceName = ReplaceName.getText();
        String replaceNumber = ReplaceNumber.getText();

        if (replaceName == null || replaceNumber == null) {
            return;
        }

        if (replaceName.isEmpty() || replaceNumber.isEmpty()) {
            return;
        }

        if (PhoneBookClient.PhoneDirecotryInterface.getPhoneNumber(replaceName) == null) {
            return;
        }

        PhoneBookClient.PhoneDirecotryInterface.replacePhoneNumber(replaceName, replaceNumber);
        ReplaceName.setText("");
        ReplaceNumber.setText("");
    }
}
